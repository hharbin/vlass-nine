import numpy as np
import os
import math
import aplpy
import pickle
import lxml.html
import matplotlib.pyplot as plt
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.io import fits
from astropy import wcs

"""
def display_image(size=0.05, savefigure=False):
    
    Display image function allows the user to input their own VLASS image and display a cutout of a given size

    User inputs include:
    Filename: string, "VLASS...fits"
    RA: floating point, gets converted to decimal degrees
    Dec: floating point, gets converted to decimal degrees
    size: floating point, decimal degrees with default set to 3.0/60.0 arcminutes (0.05)
    

    # User input of filename
    radio_filename=input("Please input the file name of your radio image .fits file: ")

    # User input of coordinates of object in decimal degrees
    RA=input("Please input the right ascension of specified object: ")
    Dec=input("Please input the declination of specificed object: ")

    # User input of image size
    size=input("Please input an image size in decimal degrees (or hit enter to use default size of 0.05 arcminutes): ")

    if size is "":
        size=0.05
        print("Using default size of {!s}".format(size))

    # If the user chooses to save the figure:
    if savefigure:
        plt.savefig('myfile.png')

    return radio_filename, RA, Dec, float(size)

radio_filename, RA, Dec, size=display_image()

c = SkyCoord(RA, Dec, frame='icrs') 
ra = c.ra.value
dec = c.dec.value

fig = plt.figure(facecolor='w', edgecolor='w', frameon=True, figsize=(12,14))
ax1 = aplpy.FITSFigure(radio_filename, dimensions=[0,1], figure=fig)

# Try/except statement to correct for the fact that VLASS data is 4D, but other radio .fits files may be 2D
try:   
    imdata = fits.getdata(radio_filename)[0,0,:,:]
except Exception as e:
    imdata = fits.getdata(radio_filename)[:,:]
    
ax1.show_colorscale(cmap='inferno', interpolation='nearest')
ax1.add_grid()
ax1.set_tick_labels_format(xformat='hh:mm:ss', yformat='dd:mm')
ax1.set_tick_color('white')
ax1.show_colorbar()

plt.show()


def radio_contours(radio_filename, RA, Dec, size):
    
    Radio contours will outline the contours of the radio portion of the image on top
    of the optical image to demonstrate that different kinds of light are not visible.

    Inputs:
    Filename: string, "VLASS...fits"
    RA: floating point, gets converted to decimal degrees
    Dec: floating point, gets converted to decimal degrees
    size: floating point, decimal degrees with default set to 3.0/60.0 arcminutes (0.05)
    Optical Filename: string, "...fits"
    Contour color: color of radio contour
    

    # User input of optical filename
    optical_filename=input("Please input the file name of your optical .fits file: ")
    
    # User input of radio filename for contours, RA and Dec, and image size will be used from previous function

    # User input of radio contour color
    color=input("Please input the color you would like the radio contours to be (choices are __) : ")

    fig = plt.figure(facecolor='w', edgecolor='w', frameon=True, figsize=(12,14))
    ax1 = aplpy.FITSFigure(optical_filename, figure=fig)
    ax1.show_colorscale(cmap='Blues')

    # Get image data
    imdata=fits.getdata(radio_filename)[0,0,:,:]

   

    plt.show()

    return optical_filename

optical_filename=radio_contours(radio_filename, RA, Dec, size)

"""

def subplot(savefigure=False):
    """
    This subplot will show a cutout of an optical image of a specific
    region specified by the user.
    It will show the optical image with the radio contours around the regions
    where radio light is found using VLASS/other radio data. It will display both the
    original VLASS or other radio .fits image as well as an optical cutout
    from an optical .fits image specified by the user. 
    """


    # User input of filename
    radio_filename=input("Please input the file name of your radio image .fits file: ")

    # User input of optical filename
    optical_filename=input("Please input the file name of your optical .fits file: ")

    # User input of coordinates of object in decimal degrees
    RA=input("Please input the right ascension of specified object: ")
    Dec=input("Please input the declination of specificed object: ")

    # User input of radio contour color
    color=input("Please input the color you would like the radio contours to be: ")

    # Optional user input of image size
    size=input("Please input an image size in decimal degrees (or hit enter to use default size of 0.05 arcminutes): ")

    if size is "":
        size=0.05
        print("Using default size of {!s}".format(size))

    # If the user chooses to save the figure:
    if savefigure:
        plt.savefig('myfile.png')

    c = SkyCoord(RA, Dec, frame='icrs') 
    ra = c.ra.value
    dec = c.dec.value

    # Try/except statement to correct for the fact that VLASS data is 4D, but other radio .fits files may be 2D
    try:   
        imdata = fits.getdata(radio_filename)[0,0,:,:]
    except Exception as e:
        imdata = fits.getdata(radio_filename)[:,:]
    
    fig = plt.figure(facecolor='w', edgecolor='w', frameon=True, figsize=(12,6))
    ax1 = aplpy.FITSFigure(radio_filename, dimensions=[0,1], figure=fig, subplot=[0.08,0.1,0.4,0.75])
    #ax1.recenter(RA, DEC, width=size, height=size)  # decimal degrees (cutout part... don't need since we want whole radio image)
    imdata = fits.getdata(radio_filename)[0,0,:,:]
    ax1.show_colorscale(cmap='inferno', vmin=0.1*np.min(imdata), vmax=0.1*np.max(imdata), interpolation='nearest')
    ax1.axis_labels.set_xtext(u'$\\mathrm{Right}$'+' '+u'$\\mathrm{Ascension}$' + ' ' +u'$\\mathrm{(J2000)}$')
    ax1.axis_labels.set_ytext(u'$\\mathrm{Declination}$'+' '+u'$\\mathrm{(J2000)}$')
    ax1.set_tick_labels_format(xformat='hh:mm:ss', yformat='dd:mm')
    ax1.set_tick_color('white')
    ax1.set_tick_labels_font(size='x-small')
    ax1.set_axis_labels_font(size='x-small')

    ax1.show_colorbar()
    ax1.colorbar.set_location('top')
    ax1.colorbar.set_axis_label_text(u'$\\mathrm{Jy/beam}$')
    ax1.colorbar.set_axis_label_font(size='xx-small')
    ax1.show_rectangles(ra, dec, size, size, edgecolor='white',facecolor='none',linewidth=2)
    
    # Optical image
    #optical_filename = radio_contours(ra, dec, size)
    ax2 = aplpy.FITSFigure(optical_filename, figure=fig, subplot=[0.55,0.1,0.36,0.72])
    ax2.show_colorscale(cmap='bone', interpolation='nearest')
    ax2.axis_labels.set_xtext(u'$\\mathrm{Right}$'+' '+u'$\\mathrm{Ascension}$' + ' ' +u'$\\mathrm{(J2000)}$')
    ax2.axis_labels.set_ytext(u'$\\mathrm{Declination}$'+' '+u'$\\mathrm{(J2000)}$')
    ax2.set_tick_labels_format(xformat='hh:mm:ss', yformat='dd:mm')
    ax2.set_tick_labels_font(size='x-small')
    ax2.set_axis_labels_font(size='x-small')

     # Selecting region for calculating rms
    rms=np.std(imdata[2900:3000, 2900:3000])

    # Determine the np.array numbers from DS9 specifcations of the radio file
    ax2.show_contour(radio_filename, colors=color, lw=0.5, levels=np.array([4.0,7.0,20.0,80.0,100.0])*rms)

    plt.show()
    
subplot()

