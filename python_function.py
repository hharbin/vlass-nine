import numpy as np
import os
import math
import aplpy
import pickle
import lxml.html
import matplotlib.pyplot as plt
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.io import fits
from astropy import wcs


def display_image(size=0.05, savefigure=False):
    """
    Display image function allows the user to input their own VLASS image and display a cutout of a given size

    User inputs include:
    Filename: string, "VLASS...fits"
    RA: floating point, decimal degrees
    Dec: floating point, decimal degrees
    size: floating point, decimal degrees with default set to 3.0/60.0 arcminutes (0.05)
    """

    # User input of filename
    filename=input("Please input the file name of your .fits file: ")

    # User input of coordinates of object in decimal degrees
    RA=input("Please input the right ascension of specified object: ")
    Dec=input("Please input the declination of specificed object: ")

    # User input of image size
    size=input("Please input an image size in decimal degrees (or hit enter to use default size): ")

    if size is "":
        size=0.05
        print("Using default size of {!s}".format(size))

    # If the user chooses to save the figure:
    if savefigure:
        plt.savefig('myfile.png')

    return filename, RA, Dec, float(size)


filename, RA, Dec, size=display_image()

c = SkyCoord(RA, Dec, frame='icrs') 
ra = c.ra.value
dec = c.dec.value


fig = plt.figure(facecolor='w', edgecolor='w', frameon=True, figsize=(12,14))
ax1 = aplpy.FITSFigure(filename, dimensions=[0,1], figure=fig)
ax1.recenter(ra, dec, width=size, height=size)  # decimal degrees

try: 
    imdata = fits.getdata(filename)[0,0,:,:]
except Exception as e:
    imdata = fits.getdata(filename)[:,:]
# ax1.show_colorscale(cmap='fusion', vmin=0.6*np.min(imdata), vmax=0.5*np.max(imdata))
ax1.show_colorscale(cmap='inferno', interpolation='nearest')
ax1.add_grid()
#ax1.axis_labels.set_xtext(u'$\\mathrm{Right}$'+' '+u'$\\mathrm{Ascension}$' + ' ' +u'$\\mathrm{(J2000)}$')
#ax1.axis_labels.set_ytext(u'$\\mathrm{Declination}$'+' '+u'$\\mathrm{(J2000)}$')
ax1.set_tick_labels_format(xformat='hh:mm:ss', yformat='dd:mm')
ax1.set_tick_color('white')
#ax1.set_tick_labels_style('latex')
#ax1.set_labels_latex(True)
# ax1.show_beam(edgecolor='white', facecolor='none', lw=1)
ax1.show_colorbar()
#ax1.colorbar.set_axis_label_text(u'$\\mathrm{Jy/beam}$')

#fig.canvas.draw()
plt.show()

