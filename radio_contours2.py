import numpy as np
import os
import math
import aplpy
import pickle
import lxml.html
import matplotlib.pyplot as plt
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.io import fits
from astropy import wcs

def subplot(radio_filename=None, optical_filename=None, RA=None, Dec=None, color=None, size=None, savefigure=False):
    """
    This subplot will show a cutout of an optical image of a specific
    region specified by the user.
    It will show the optical image with the radio contours around the regions
    where radio light is found using VLASS/other radio data. It will display both the
    original VLASS or other radio .fits image as well as an optical cutout
    from an optical .fits image specified by the user. 
    """


    # User input of filename
    if radio_filename is None:
        radio_filename=input("Please input the file name of your radio image .fits file: ")

    # User input of optical filename
    if optical_filename is None:
        optical_filename=input("Please input the file name of your optical .fits file: ")

    # User input of coordinates of object in decimal degrees
    if RA is None:
        RA=input("Please input the right ascension of specified object: ")
    if Dec is None:
        Dec=input("Please input the declination of specificed object: ")

    # User input of radio contour color
    if color is None:
        color=input("Please input the color you would like the radio contours to be: ")

    # Optional user input of image size
    if size is None:
        size=input("Please input an image size in decimal degrees (or hit enter to use default size of 0.05 degrees = 3 arcminutes): ")

    if size is "":
        size=0.05
        print("Using default size of {!s}".format(size))

    

    c = SkyCoord(RA, Dec, frame='icrs') 
    ra = c.ra.value
    dec = c.dec.value

    # Try/except statement to correct for the fact that VLASS data is 4D, but other radio .fits files may be 2D
    try:   
        imdata = fits.getdata(radio_filename)[0,0,:,:]
    except Exception as e:
        imdata = fits.getdata(radio_filename)[:,:]

    #import pdb;pdb.set_trace()
    
    fig = plt.figure(facecolor='w', edgecolor='w', frameon=True, figsize=(12,6))
    ax1 = aplpy.FITSFigure(radio_filename, dimensions=[0,1], figure=fig, subplot=[0.08,0.1,0.4,0.75])
    #ax1.recenter(RA, DEC, width=size, height=size)  # decimal degrees (cutout part... don't need since we want whole radio image)
    #ax1.show_colorscale(cmap='inferno', vmin=0.1*np.min(imdata), vmax=0.1*np.max(imdata), interpolation='nearest')
    ax1.show_colorscale(cmap='inferno', interpolation='nearest')
    ax1.axis_labels.set_xtext(u'$\\mathrm{Right}$'+' '+u'$\\mathrm{Ascension}$' + ' ' +u'$\\mathrm{(J2000)}$')
    ax1.axis_labels.set_ytext(u'$\\mathrm{Declination}$'+' '+u'$\\mathrm{(J2000)}$')
    ax1.set_tick_labels_format(xformat='hh:mm:ss', yformat='dd:mm')
    ax1.set_tick_color('white')
    ax1.set_tick_labels_font(size='x-small')
    ax1.set_axis_labels_font(size='x-small')

    ax1.show_colorbar()
    ax1.colorbar.set_location('top')
    ax1.colorbar.set_axis_label_text(u'$\\mathrm{Jy/beam}$')
    ax1.colorbar.set_axis_label_font(size='xx-small')

    hdulist = fits.open(radio_filename)

    if 'Vlass' in hdulist[0].header['OBSERVER']:
        try:
            ax1.show_rectangles(ra, dec, size, size, edgecolor='white',facecolor='none',linewidth=2)
        except:
            print("Notification: Unable to draw image box.  Placing circle marker on image.")
            ax1.show_circles(ra, dec, size/3.0, wdgecolor='white', facecolor='none', linewidth=2)
        finally:
            print("Warning:  Will not draw overlay shapes on image")
            
    
    # Optical image
    #optical_filename = radio_contours(ra, dec, size)
    ax2 = aplpy.FITSFigure(optical_filename, figure=fig, subplot=[0.55,0.1,0.36,0.72])
    ax2.show_colorscale(cmap='bone', interpolation='nearest')
    ax2.axis_labels.set_xtext(u'$\\mathrm{Right}$'+' '+u'$\\mathrm{Ascension}$' + ' ' +u'$\\mathrm{(J2000)}$')
    ax2.axis_labels.set_ytext(u'$\\mathrm{Declination}$'+' '+u'$\\mathrm{(J2000)}$')
    ax2.set_tick_labels_format(xformat='hh:mm:ss', yformat='dd:mm')
    ax2.set_tick_labels_font(size='x-small')
    ax2.set_axis_labels_font(size='x-small')

     # Selecting region for calculating rms
    #hdulist = fits.open(self.filename)
    max_pix = int(hdulist[0].header['NAXIS1'])
    rms=np.std(imdata[20:30, 20:30])
    
    print('RMS Value: {!s}'.format(str(rms)))

    # Determine the np.array numbers from DS9 specifcations of the radio file
    ax2.show_contour(radio_filename, colors=color, levels=np.array([4.0,7.0,20.0,80.0,100.0])*rms)

    plt.show()
    
    # If the user chooses to save the figure:
    if savefigure:
        plt.savefig('myfile.png')
        
    return "Subplot function executed."
    
#subplot()

if __name__ == '__main__':
    radio_filename = 'VLASS1.1.ql.T08t23.J151033-113000.10.2048.v1.I.iter1.image.pbcor.tt0.subim.fits'
    optical_filename = 'skv4044661384433_1.fits'
    RA = '15h11m40.416s'
    Dec = '-11d10m59.88s'
    color = 'white'
    size = 0.1
    
    subplot(radio_filename=radio_filename,
            optical_filename=optical_filename,
            RA=RA,
            Dec=Dec,
            color=color,
            size=size)

