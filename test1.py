import unittest
import datetime
from astropy.io import fits
from astropy.time import Time
from astropy.coordinates import SkyCoord
from python_function import display_image

class test_display_image(unittest.TestCase):
    def test_outputs(self):
        """This tests the format of the right ascension
        """
        self.filename, self.RA, self.Dec, self.size=display_image()
        self.assertIsInstance(self.RA,str)
        self.assertIsInstance(self.Dec,str)
        self.assertIsInstance(self.filename,str)

    def test_coords(self):
        """Test RA and Dec conversion
        """
        self.filename = 'dots.fits'
        hdulist = fits.open(self.filename)
        ra_deg = hdulist[0].header['CRVAL1']
        dec_deg = hdulist[0].header['CRVAL2']
        self.assertTrue(0.0 <= float(ra_deg) <= 360.0)
        self.assertTrue(-40.0 <= float(dec_deg) <= 90.0)
        #-40.0 degrees is minimum for VLASS

    def test_metadata(self):
        """Test metadata of VLASS .fits file
        """
        self.filename = 'dots.fits'

        #frequency testing
        hdulist = fits.open(self.filename)

        CTYPE3_name = hdulist[0].header['CTYPE3']
        self.assertEqual(CTYPE3_name, 'FREQ')

        CRVAL3_name = hdulist[0].header['CRVAL3']
        self.assertTrue(2.0E9 < float(CRVAL3_name) < 4.0E9)
        
        #image size
        hdulist = fits.open(self.filename)

        NAXIS1_name = hdulist[0].header['NAXIS1']
        self.assertEqual(int(NAXIS1_name), 3722)

        NAXIS2_name = hdulist[0].header['NAXIS2']
        self.assertEqual(int(NAXIS2_name), 3722)

        #pixel scale
        hdulist = fits.open(self.filename)

        CDELT1_name = hdulist[0].header['CDELT1']
        self.assertEqual(float(CDELT1_name), -2.777777777778E-04)

        CDELT2_name = hdulist[0].header['CDELT2']
        self.assertEqual(float(CDELT2_name), 2.777777777778E-04)
        
        
        #date test -- don't want someone to use the pilot data, so must be past a certain date
        date_obs = hdulist[0].header['DATE-OBS']
        print(date_obs)
        t = Time(date_obs, format='isot', scale='utc')
        self.assertTrue(t.datetime >= datetime.datetime(2017,9,1))
        

if __name__=='__main__':
    unittest.main()

