# Welcome to my Git Repository! #

## Below is a brief overview of the code uploaded here: ##

1. This repository builds upon work done by last year's NINE cohort (a simple web access tool designed to retrieve data from VLASS) by providing simple Python command line tools compatible with Raspberry Pi modules.
2. All code committed in this repository is compatible with the Python Miniconda installation on a Raspberry Pi module (Anaconda for Raspberry Pi).
3. The code here provides simple image displays and image cutouts as well as multiwavelength comparisons of VLASS images.
4. Feel free to pull code from the repository and make your own modifications!

## Image Cutouts and Simple Image Display ##
- heather_aplpy.py provides a simple image display from VLASS .fits files.

- image_cutout.py is an image cutout program designed for use with VLASS .fits files.

- python_function.py is a similar program to image_cutout.py but with some modifications such as the use of a function and addition of user input.

## Multiwavelength Comparisons with Radio Contours ##
- radio_contours1.py is the original setup of the radio contours Python code, including the setup of separate functions.

- radio_contours.py is the second version without all previous functions (here they have been combined). 

- radio_contours2.py is the final version of radio contours code which includes the possibility for using input other than VLASS files.

## Unit Tests ##
- dots.fits and test1.py are paired unit test and test file.